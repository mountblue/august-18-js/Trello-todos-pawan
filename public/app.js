
$(document).ready(() => {
  const authToken = '?key=d842e0fd4be8b10c062ffd378607b856&token=3fee1ad068df23bb68bd6938afd4de28589fdbf0264a6b13f066239a22fcc264';
  // const boardsRequestUrl = `https://api.trello.com/1/members/me/boards${authToken}`
  boardDetails();
  function boardDetails() {
    const boardsRequestUrl = `https://api.trello.com/1/boards/5b9104c65ea1ee7d0267f556${authToken}`;
    $.getJSON(boardsRequestUrl, (board) => {
      cardDetails(board);
    });
  }
  function cardDetails(board) {
    const cardUrl = `https://api.trello.com/1/boards/${board.id}/cards${authToken}`;
    $.getJSON(cardUrl, (cards) => {
      cards.forEach((card) => {
        checkListDetails(card);
      });
    });
  }
  function checkListDetails(card) {
    const listUrl = `https://api.trello.com/1/cards/${card.id}/checklists${authToken}`;
    $.getJSON(listUrl, (checkLists) => {
      checkLists.forEach((checkList) => {
        const checkListItems = checkList.checkItems;
        checkItemsDetails(checkListItems, card);
      });
    });
  }
  function checkItemsDetails(checkItems, card) {
    checkItems.forEach((checkListItem) => {
      const state = checkListItem.state;
      let checkListItemName = checkListItem.name;
      let cardName = card.name;
      const checkListItemId = checkListItem.id;
      const cardId = card.id;
      let status;
      if (state == 'complete') {
        status = 'checked';
        checkListItemName = checkListItemName.strike();
        cardName = cardName.strike();
      }
      if (state == 'incomplete') {
        status = '';
      }
      $('#display').append(`<input type="checkbox" data-cardId="${cardId}" data-checkListItemId="${checkListItemId}" data-checkListItemName="${checkListItemName}" data-cardName="${cardName}" class="checkbox" ${status}/><span id="${checkListItemId}" class="checkListData">${checkListItemName}<span class="card-details">${cardName}</span></span><br><br>`);
    });
  }
  // updating the trello api
  $(document).on('change', 'input[type=checkbox]', function () {
    const cardId = $(this).attr('data-cardId');
    const checkListItemId = $(this).attr('data-checkListItemId');
    let checkListItem = $(this).attr('data-checkListItemName');
    let cardName = $(this).attr('data-cardName');
    let status;
    if (this.checked) {
      checkListItem = checkListItem.strike();
      cardName = cardName.strike();
      status = 'complete';
    } else {
      status = 'incomplete';
    }
    $(`span#${checkListItemId}`).html(`<span id="${checkListItemId}">${checkListItem}<span class="card-details">${cardName}</span></span>`);
    $.ajax({
      url: `https://api.trello.com/1/cards/${cardId}/checkItem/${checkListItemId}?state=${status}&key=d842e0fd4be8b10c062ffd378607b856&token=3fee1ad068df23bb68bd6938afd4de28589fdbf0264a6b13f066239a22fcc264`,
      method: 'PUT',
    });
  });
});
